package com.example.kenan.filmovi;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by Kenan on 17.05.2017..
 */

public class SearchInfo extends AsyncTask<String, Integer, Void> {
    public interface OnInfoSearchDone
    {
        public void onDone(ArrayList<Reziser> reziseri, ArrayList<Zanr> zanrovi);
    }
    ArrayList<Reziser> reziseri;
    ArrayList<Zanr> zanrovi;
    private OnInfoSearchDone pozivatelj;
    public SearchInfo(OnInfoSearchDone p){pozivatelj = p;}
    @Override
    protected Void doInBackground(String... params)
    {
        String query = null;
        try
        {
            query = URLEncoder.encode(params[0],"utf-8");
        }
        /*catch(MalformedURLException e)
        {
            e.printStackTrace();
        }*/
        catch(UnsupportedEncodingException e)
        {
            // :(
        }

        String url1 = "https://api.themoviedb.org/3/person/" + query + "/movie_credits?api_key=ccf4aada7fe42c0ab1d8f42356aad70f";
        try
        {
            reziseri = new ArrayList<Reziser>();
            zanrovi  = new ArrayList<Zanr>();

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("cast");

            for(int i = items.length() - 1; i >= 0; i--)
            {
                // Dohvacanje zanrova
                JSONObject movie = items.getJSONObject(i);
                String id = movie.getString("id");

                String movieURL = "https://api.themoviedb.org/3/movie/" + id + "?api_key=ccf4aada7fe42c0ab1d8f42356aad70f";

                URL glumciURL = new URL(movieURL);
                HttpURLConnection urlConnection2 = (HttpURLConnection)glumciURL.openConnection();
                InputStream stream = new BufferedInputStream(urlConnection2.getInputStream());

                String rezultatFilm = convertStreamToString(stream);

                JSONObject film = new JSONObject(rezultatFilm);
                JSONArray zanroviJSON = film.getJSONArray("genres");

                int j = 0;
                for(; j < zanroviJSON.length(); j++)
                    if(!duplikat(zanroviJSON.getJSONObject(j).getString("name"), zanrovi)) {
                        zanrovi.add(new Zanr(zanroviJSON.getJSONObject(j).getString("name")));
                        break;
                    }
                if(zanrovi.size() == 7)
                    break;
            }
            for(int i = 0; i < items.length(); i++)
            {
                // Dohvacanje rezisera
                if(reziseri.size() >= 7)
                    break;

                JSONObject movie = items.getJSONObject(i);
                String id = movie.getString("id");

                String movieURL = "https://api.themoviedb.org/3/movie/" + id + "/credits?api_key=ccf4aada7fe42c0ab1d8f42356aad70f";

                URL glumciURL = new URL(movieURL);
                HttpURLConnection urlConnection2 = (HttpURLConnection)glumciURL.openConnection();
                InputStream stream = new BufferedInputStream(urlConnection2.getInputStream());

                String rezultatFilm = convertStreamToString(stream);

                JSONObject film = new JSONObject(rezultatFilm);

                JSONArray crew = film.getJSONArray("crew");
                for(int j = 0; j < crew.length(); j++) {
                    JSONObject osoba = crew.getJSONObject(j);
                    if(osoba.getString("job").equals("Director")) {
                        if (!duplikatReziser(osoba.getString("name"), reziseri))
                            reziseri.add(new Reziser(osoba.getString("name")));
                    }
                }
            }
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    private boolean duplikat(String vrijednost, ArrayList<Zanr> zanroviLista)
    {
        for(int i = 0; i < zanroviLista.size();i++)
        {
            if(vrijednost.equals(zanroviLista.get(i).dajNaziv()))
                return true;
        }
        return false;
    }
    private boolean duplikatReziser(String vrijednost, ArrayList<Reziser> reziseriLista)
    {
        for(int i = 0; i < reziseriLista.size();i++)
        {
            if(vrijednost.equals(reziseriLista.get(i).dajIme()))
                return true;
        }
        return false;
    }
    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(reziseri,zanrovi);
    }

    public String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        }
        catch(IOException e)
        {

        }
        finally
        {
            try
            {
                is.close();
            }
            catch(IOException e)
            {

            }
        }
        return sb.toString();
    }
}
