package com.example.kenan.filmovi;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Kenan on 14.06.2017..
 */

public class FragmentDetaljiFilmovi extends Fragment {
    Film f;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate) {
        View iv = inflater.inflate(R.layout.fragment_detalji_filmovi,container,false);
        final TextView naziv = (TextView)iv.findViewById(R.id.textView);
        final EditText text = (EditText)iv.findViewById(R.id.editText);
        final DatePicker date = (DatePicker)iv.findViewById(R.id.datePicker);
        final Button spasi = (Button)iv.findViewById(R.id.buttonSpasi);
        if(getArguments()!=null && getArguments().containsKey("film")) {

            f = getArguments().getParcelable("film");
            naziv.setText(f.dajNaziv());
        }

        spasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    long calID = 1;
                    long startMillis = 0;
                    long endMillis = 0;
                    Calendar beginTime = Calendar.getInstance();
                    beginTime.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
                    startMillis = beginTime.getTimeInMillis();
                    Calendar endTime = Calendar.getInstance();
                    endTime.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
                    endMillis = endTime.getTimeInMillis();

                    ContentResolver cr = getActivity().getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(Events.DTSTART, startMillis);
                    values.put(Events.DTEND, endMillis);
                    values.put(Events.TITLE, naziv.getText().toString());
                    values.put(Events.DESCRIPTION, text.getText().toString());
                    values.put(Events.CALENDAR_ID, calID);
                    values.put(Events.EVENT_TIMEZONE, "Europe/Sarajevo");
                    Uri uri = cr.insert(Events.CONTENT_URI, values);

                    long eventID = Long.parseLong(uri.getLastPathSegment());
                    //"Ovaj događaj može služiti kao podsjetnik za korisnika da pogleda navedeni film."
                    Log.d("Dodan event: ", "Event ID: " + eventID + " vrijednotsi : " + values.toString());

                    ContentValues values2 = new ContentValues();
                    values2.put(CalendarContract.Reminders.MINUTES, 15);
                    values2.put(CalendarContract.Reminders.EVENT_ID, eventID);
                    values2.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                    Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, values2);
                }
                catch(SecurityException e)
                {
                    // insecure :(
                    Log.d("Izuzetak","Desio se security izuzetak");
                }

            }
        });


        return iv;
    }
}