package com.example.kenan.filmovi;


import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_BIOGRAFIJA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_DATUM;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_IME;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_MJESTO;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ONLINE_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REJTING;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REZISER_GLUMAC;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REZISER_REZISER;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SLIKA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SPOL;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_URL;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_GLUMCI_REZISERI;
import static com.example.kenan.filmovi.GlumacDBHelper.ZANR_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.ZANR_NAZIV;


/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentListaGlumci extends Fragment implements SearchArtist.OnGlumacSearchDone {

    public interface OnItemClick{
        public void onItemClicked(int pos);
    }
    ArrayList<Glumac> glumci;
    AdapterGlumac adapter;

    private OnItemClick oic;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        View iv = inflater.inflate(R.layout.fragment_lista_glumci,container,false);
        final Button dugme = (Button)iv.findViewById(R.id.buttonTrazi);
        final EditText text = (EditText)iv.findViewById(R.id.editText);

        dugme.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(text.getText().toString().contains("actor:")) {
                    String pretraga = text.getText().toString().substring(text.getText().toString().indexOf(':')+1);
                    String[] koloneRezulat = new String[]{ "DISTINCT *"};
                    // Specificiramo WHERE dio upita
                    String where = GLUMAC_IME + " LIKE '%"+pretraga+"%'";
                    // Definišemo argumente u where upitu, group by, having i order po potrebi
                    String whereArgs[] = null;
                    String groupBy = null;
                    String having = null;
                    String order = null;
                    // Dohvatimo referencu na bazu (poslije ćemo opisati kako se implementira helper)
                    SQLiteDatabase db = new GlumacDBHelper(getActivity()).getWritableDatabase();
                    // Izvršimo upit
                    Cursor cursor = db.query(GlumacDBHelper.TABLE_GLUMCI,koloneRezulat, where, whereArgs, groupBy, having, order);
                    glumci.clear();
                    cursor.moveToFirst();
                    for(int i = 0; i < cursor.getCount();i++)
                    {
                        String oid = cursor.getString(cursor.getColumnIndex(GLUMAC_ONLINE_ID));
                        String ime = cursor.getString(cursor.getColumnIndex(GLUMAC_IME));
                        String spol = cursor.getString(cursor.getColumnIndex(GLUMAC_SPOL));
                        String datum = cursor.getString(cursor.getColumnIndex(GLUMAC_DATUM));
                        String mjesto = cursor.getString(cursor.getColumnIndex(GLUMAC_MJESTO));
                        String rejting = cursor.getString(cursor.getColumnIndex(GLUMAC_REJTING));
                        String url = cursor.getString(cursor.getColumnIndex(GLUMAC_URL));
                        String biografija = cursor.getString(cursor.getColumnIndex(GLUMAC_BIOGRAFIJA));
                        String slika = cursor.getString(cursor.getColumnIndex(GLUMAC_SLIKA));
                        Glumac g = new Glumac(oid, ime, slika,spol,datum,mjesto,rejting,url,biografija);
                        glumci.add(g);
                        cursor.moveToNext();
                    }
                    adapter.notifyDataSetChanged();
                }
                else if(text.getText().toString().contains("director:")) {
                    SQLiteDatabase db = new GlumacDBHelper(getActivity()).getWritableDatabase();
                    String pretraga = text.getText().toString().substring(text.getText().toString().indexOf(':')+1);
                    Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_GLUMCI_REZISERI +" WHERE " + GLUMAC_REZISER_REZISER + " LIKE '%" + pretraga + "%';", null);
                    cursor.moveToFirst();
                    // Izvršimo upit
                    glumci.clear();
                    for(int i = 0; i < cursor.getCount();i++)
                    {
                        String pretraga2 = cursor.getString(cursor.getColumnIndex(GLUMAC_REZISER_GLUMAC));
                        String[] koloneRezulat = new String[]{ "DISTINCT *"};
                        String where = GLUMAC_ONLINE_ID + " ="+pretraga2+";";
                        String whereArgs[] = null;
                        String groupBy = null;
                        String having = null;
                        String order = null;


                        Cursor cursor2 = db.query(GlumacDBHelper.TABLE_GLUMCI,koloneRezulat, where, whereArgs, groupBy, having, order);
                        cursor2.moveToFirst();
                        Log.d("LOG1: ","SELECT * FROM " + TABLE_GLUMCI_REZISERI +" WHERE " + GLUMAC_REZISER_REZISER + " LIKE '%" + pretraga + "%';");
                        Log.d("LOG2: ",GLUMAC_ID + " ="+pretraga2+"; - vraceno : " + cursor2.getCount());
                        for(int j = 0; j < cursor2.getCount();j++) {
                            String oid = cursor2.getString(cursor2.getColumnIndex(GLUMAC_ONLINE_ID));
                            String ime = cursor2.getString(cursor2.getColumnIndex(GLUMAC_IME));
                            String spol = cursor2.getString(cursor2.getColumnIndex(GLUMAC_SPOL));
                            String datum = cursor2.getString(cursor2.getColumnIndex(GLUMAC_DATUM));
                            String mjesto = cursor2.getString(cursor2.getColumnIndex(GLUMAC_MJESTO));
                            String rejting = cursor2.getString(cursor2.getColumnIndex(GLUMAC_REJTING));
                            String url = cursor2.getString(cursor2.getColumnIndex(GLUMAC_URL));
                            String biografija = cursor2.getString(cursor2.getColumnIndex(GLUMAC_BIOGRAFIJA));
                            String slika = cursor2.getString(cursor2.getColumnIndex(GLUMAC_SLIKA));
                            Glumac g = new Glumac(oid, ime, slika, spol, datum, mjesto, rejting, url, biografija);
                            glumci.add(g);
                            cursor2.moveToNext();
                        }
                        cursor.moveToNext();
                    }
                    adapter.notifyDataSetChanged();

                }
                else {
                    new SearchArtist((SearchArtist.OnGlumacSearchDone) FragmentListaGlumci.this).execute(text.getText().toString());
                    //adapter.notifyDataSetChanged();
                    text.setText("");
                }
            }
        });

        return iv;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(getArguments().containsKey("Alista"))
        {
            glumci = new ArrayList<Glumac>();
            glumci = getArguments().getParcelableArrayList("Alista");
            ListView lv = (ListView)getView().findViewById(R.id.listaGlumci);
            adapter = new AdapterGlumac(getActivity(),R.layout.lista_glumaca,glumci);
            lv.setAdapter(adapter);


            try{
                oic = (OnItemClick)getActivity();
            }
            catch(ClassCastException e){
                throw new ClassCastException(getActivity().toString()+"Treba implementirati OnItemClick");
            }

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                    oic.onItemClicked(position);
                }
            });
        }
    }
    @Override
    public void onDone(ArrayList<Glumac> m)
    {
        glumci.clear();
        glumci.addAll(m);
        adapter.notifyDataSetChanged();
    }
}
