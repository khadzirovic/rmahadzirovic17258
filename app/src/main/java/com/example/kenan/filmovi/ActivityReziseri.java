package com.example.kenan.filmovi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Kenan on 31.03.2017..
 */

public class ActivityReziseri extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reziseri);

        ListView lista = (ListView) findViewById(R.id.listaReziseri);
        /* HARDKODIRANI PODACI*/
        ArrayList<Reziser> unosi = new ArrayList<Reziser>();
        Reziser r1 = new Reziser("Quentin Tarantino");
        Reziser r2 = new Reziser("Brian De Palma");
        Reziser r3 = new Reziser("Stephen Spielberg");
        unosi.add(r1);
        unosi.add(r2);
        unosi.add(r3);
        /* KRAJ HARDKODIRANJA PODATAKA */

        final ArrayAdapter<Reziser> adapter;
        adapter = new ArrayAdapter<Reziser>(this,android.R.layout.simple_list_item_1, unosi);

        lista.setAdapter(adapter);

        Button dugmeGlumci = (Button) findViewById(R.id.buttonGlumci);
        Button dugmeReziseri = (Button) findViewById(R.id.buttonReziseri);
        Button dugmeZanrovi = (Button) findViewById(R.id.buttonZanrovi);

        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActivityReziseri.this, MainActivity.class);
                ActivityReziseri.this.startActivity(myIntent);
            }
        });

        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActivityReziseri.this, ActivityZanrovi.class);
                ActivityReziseri.this.startActivity(myIntent);
            }
        });
    }
}