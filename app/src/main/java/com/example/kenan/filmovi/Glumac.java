package com.example.kenan.filmovi;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kenan on 31.03.2017..
 */

public class Glumac implements Parcelable{

    // ************** Atributi **************
    private String Id;
    private String Ime;
    private String Slika;
    private String Spol;
    private String Datum;
    private String Mjesto;
    private String Rejting;
    private String Url;
    private String Biografija;

    // *************** Set metode **************
    public void postaviId(String id) {this.Id = id;}
    public void postaviIme(String ime)
    {
        this.Ime = ime;
    }
    public void postaviSpol(String spol)
    {
        this.Spol = spol;
    }
    public void postaviSliku(String slika)
    {
        this.Slika = slika;
    }
    public void postaviDatum(String datum)
    {
        this.Datum = datum;
    }
    public void postaviMjesto(String mjesto)
    {
        this.Mjesto = mjesto;
    }
    public void postaviUrl(String url)
    {
        this.Url = url;
    }
    public void postaviRejting(String rejting)
    {
        this.Rejting = rejting;
    }
    public void postaviBiografiju(String biografija){
        this.Biografija = biografija;
    }

    // *********** Get Metode ****************
    public String dajId()  {return this.Id;}
    public String dajIme()
    {
        return this.Ime;
    }
    public String dajSliku()
    {
        return this.Slika;
    }
    public String dajSpol()
    {
        return this.Spol;
    }
    public String dajUrl()
    {
        return this.Url;
    }
    public String dajDatum()
    {
        return this.Datum;
    }
    public String dajMjesto()
    {
        return this.Mjesto;
    }
    public String dajRejting()
    {
        return this.Rejting;
    }
    public String dajBiografiju()
    {
        return this.Biografija;
    }
    public int dajBoju() { if(dajSpol().equals("Muski")) return Color.rgb(131, 177, 252); else return Color.rgb(255, 132, 177); }

    // *********** Konstruktori *************
    public Glumac()
    {
        this.Id = "";
        this.Ime = "";
        this.Slika = "";
        this.Spol = "";
        this.Datum = "";
        this.Mjesto = "";
        this.Rejting = "";
        this.Url = "";
        this.Biografija = "";
    }

    protected Glumac(Parcel parcel)
    {
        Id = parcel.readString();
        Ime = parcel.readString();
        Slika = parcel.readString();
        Spol = parcel.readString();
        Datum = parcel.readString();
        Mjesto = parcel.readString();
        Rejting = parcel.readString();
        Url = parcel.readString();
        Biografija = parcel.readString();
    }

    public static final Parcelable.Creator<Glumac> CREATOR = new Parcelable.Creator<Glumac>()
    {
        @Override
        public Glumac createFromParcel(Parcel parcel)
        {
            return new Glumac(parcel);
        }
        @Override
        public Glumac[] newArray(int size)
        {
            return new Glumac[size];
        }
    };

    public Glumac(Glumac g)
    {
        this.Id = g.Id;
        this.Ime = g.Ime;
        this.Slika = g.Slika;
        this.Spol = g.Spol;
        this.Datum = g.Datum;
        this.Mjesto = g.Mjesto;
        this.Rejting = g.Rejting;
        this.Url = g.Url;
        this.Biografija = g.Biografija;
    }

    public Glumac(String Id, String ime, String slika, String spol, String godiste, String mjesto, String rejting, String url, String biografija)
    {
        this.Id = Id;
        this.Ime = ime;
        this.Slika = slika;
        this.Spol = spol;
        this.Datum = godiste;
        this.Mjesto = mjesto;
        this.Rejting = rejting;
        this.Url = url;
        this.Biografija = biografija;
    }
    public Glumac(String id, String ime, String spol, String godiste, String mjesto, String rejting, String url, String biografija)
    {
        this.Id = id;
        this.Ime = ime;
        this.Slika = "";
        this.Spol = spol;
        this.Datum = godiste;
        this.Mjesto = mjesto;
        this.Rejting = rejting;
        this.Url = url;
        this.Biografija = biografija;
    }

    // *********** Metode ***********
    @Override
    public int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(Id);
        dest.writeString(Ime);
        dest.writeString(Slika);
        dest.writeString(Spol);
        dest.writeString(Datum);
        dest.writeString(Mjesto);
        dest.writeString(Rejting);
        dest.writeString(Url);
        dest.writeString(Biografija);
    }

}
