package com.example.kenan.filmovi;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kenan on 31.03.2017..
 */

public class Zanr implements Parcelable{

    // *********** Atributi ***********
    private String Naziv;

    // *********** Konstruktori ***********
    public Zanr()
    {
        Naziv = "";
    }
    public Zanr(String n)
    {
        this.Naziv = n;
    }
    protected Zanr(Parcel parcel){ Naziv = parcel.readString(); }

    // *********** Set Metode ***********
    public void postaviNaziv(String Naziv)
    {
        this.Naziv = Naziv;
    }

    // *********** Get Metode ***********
    public String dajNaziv()
    {
        return this.Naziv;
    }
    public String dajSliku()
    {
        return this.Naziv.toLowerCase();
    }

    // *********** Metode ***********
    public String toString()
    {
        return this.dajNaziv();
    }
    public static final Parcelable.Creator<Zanr> CREATOR = new Parcelable.Creator<Zanr>()
    {
        @Override
        public Zanr createFromParcel(Parcel parcel)
        {
            return new Zanr(parcel);
        }
        @Override
        public Zanr[] newArray(int size)
        {
            return new Zanr[size];
        }
    };
    @Override
    public int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(Naziv);
    }
}
