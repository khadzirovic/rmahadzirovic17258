package com.example.kenan.filmovi;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Kenan on 14.06.2017..
 */

public class SearchMovie extends AsyncTask<String, Integer, Void>{
    public interface OnFilmSearchDone
    {
        public void onDone(ArrayList<Film> rez);
    }
    ArrayList<Film> rez;
    private OnFilmSearchDone pozivatelj;
    public SearchMovie(OnFilmSearchDone p){pozivatelj = p;}


    @Override
    protected Void doInBackground(String... params)
    {
        String query = null;
        try
        {
            query = URLEncoder.encode(params[0],"utf-8");
        }
        /*catch(MalformedURLException e)
        {
            e.printStackTrace();
        }*/
        catch(UnsupportedEncodingException e)
        {
            // :(
        }

        String url1 = "https://api.themoviedb.org/3/search/movie?api_key=ccf4aada7fe42c0ab1d8f42356aad70f&query=" + query;
        try
        {
            rez = new ArrayList<Film>();

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("results");

            for(int i = 0; i<items.length(); i++)
            {
                //
                JSONObject artist = items.getJSONObject(i);
                String naslov = artist.getString("title");
                rez.add(new Film(naslov));
                //

            }
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(rez);
    }

    public String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        }
        catch(IOException e)
        {

        }
        finally
        {
            try
            {
                is.close();
            }
            catch(IOException e)
            {

            }
        }
        return sb.toString();
    }

}