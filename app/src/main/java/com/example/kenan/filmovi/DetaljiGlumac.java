package com.example.kenan.filmovi;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Kenan on 31.03.2017..
 */

public class DetaljiGlumac extends AppCompatActivity {
    Glumac m;
    public DetaljiGlumac(Glumac glumac)
    {
        m = new Glumac(glumac);
    }
    public DetaljiGlumac()
    {
        m = new Glumac();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalji_glumca);

        TextView ime = (TextView)findViewById(R.id.Ime);
        TextView spol = (TextView)findViewById(R.id.Spol);
        TextView godiste = (TextView)findViewById(R.id.Godine);
        TextView biografija = (TextView)findViewById(R.id.Biografija);
        TextView url = (TextView)findViewById(R.id.Link);
        ImageView slika = (ImageView)findViewById(R.id.Slika);

        m = new Glumac(getIntent().getStringExtra("ime"),getIntent().getStringExtra("slika"),getIntent().getStringExtra("spol"),getIntent().getStringExtra("godiste"),getIntent().getStringExtra("mjesto"),getIntent().getStringExtra("rejting"),getIntent().getStringExtra("link"),getIntent().getStringExtra("biografija"));

        ime.setText(m.dajIme());
        godiste.setText(m.dajDatum());
        spol.setText(m.dajSpol());
        url.setText(m.dajUrl());
        biografija.setText(m.dajBiografiju());
        slika.setImageResource(getResources().getIdentifier("com.example.kenan.filmovi:drawable/"+m.dajSliku(),null,null));

        if(m.dajSpol().equals("Muski"))
            getWindow().getDecorView().setBackgroundColor(Color.rgb(131, 177, 252));
        else
            getWindow().getDecorView().setBackgroundColor(Color.rgb(255, 132, 177));


        url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(m.dajUrl()));
                if(browserIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(browserIntent);
            }
        });

        Button podijeli = (Button)findViewById(R.id.buttonPodijeli);
        podijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, m.dajBiografiju());
                sendIntent.setType("text/plain");

                if(sendIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(sendIntent);
            }
        });


    }

}