package com.example.kenan.filmovi;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_BIOGRAFIJA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_DATUM;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_IME;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_MJESTO;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ONLINE_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REJTING;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SLIKA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SPOL;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_URL;


/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentDetaljiGlumci  extends Fragment {
    Glumac m;
    public interface Bookmark
    {
        public void dodajBookmarkano(Glumac m);
    }
    private Bookmark i;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        //return inflater.inflate(R.layout.fragment_detalji_glumci,container,false);
        View iv = inflater.inflate(R.layout.fragment_detalji_glumci,container,false);
        TextView ime = (TextView)iv.findViewById(R.id.Ime);
        TextView spol = (TextView)iv.findViewById(R.id.Spol);
        TextView godiste = (TextView)iv.findViewById(R.id.Godine);
        TextView biografija = (TextView)iv.findViewById(R.id.Biografija);
        TextView url = (TextView)iv.findViewById(R.id.Link);
        ImageView slika = (ImageView)iv.findViewById(R.id.Slika);
        ImageButton bookmark = (ImageButton)iv.findViewById(R.id.buttonBookmark);

        if(getArguments()!=null && getArguments().containsKey("glumac")) {

            m = getArguments().getParcelable("glumac");

            ime.setText(m.dajIme());
            godiste.setText(m.dajDatum());
            spol.setText(m.dajSpol());
            url.setText(m.dajUrl());
            biografija.setText(m.dajBiografiju());
            slika.setImageResource(getResources().getIdentifier("com.example.kenan.filmovi:drawable/" + m.dajSliku(), null, null));

            Picasso.with(getActivity()).load(m.dajSliku()).into(slika);
            iv.setBackgroundColor(m.dajBoju());

        }

        url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(m.dajUrl()));
                if(browserIntent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivity(browserIntent);
            }
        });

        Button podijeli = (Button)iv.findViewById(R.id.buttonPodijeli);

        podijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, m.dajBiografiju());
                sendIntent.setType("text/plain");

                if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivity(sendIntent);
            }
        });

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues noveVrijednosti = new ContentValues();

                noveVrijednosti.put(GLUMAC_ONLINE_ID,m.dajId());
                noveVrijednosti.put(GLUMAC_IME,m.dajIme());
                noveVrijednosti.put(GLUMAC_SLIKA,m.dajSliku());
                noveVrijednosti.put(GLUMAC_SPOL,m.dajSpol());
                noveVrijednosti.put(GLUMAC_DATUM,m.dajDatum());
                noveVrijednosti.put(GLUMAC_MJESTO,m.dajMjesto());
                noveVrijednosti.put(GLUMAC_REJTING,m.dajRejting());
                noveVrijednosti.put(GLUMAC_URL,m.dajUrl());
                noveVrijednosti.put(GLUMAC_BIOGRAFIJA,m.dajBiografiju());

                SQLiteDatabase db = new GlumacDBHelper(getActivity()).getWritableDatabase();
                db.insert(GlumacDBHelper.TABLE_GLUMCI,null,noveVrijednosti);
                db.close();

                i = (Bookmark)getActivity();
                i.dodajBookmarkano(m);
            }
        });

        return iv;
    }
}