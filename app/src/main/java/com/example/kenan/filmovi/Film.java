package com.example.kenan.filmovi;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kenan on 14.06.2017..
 */

public class Film implements Parcelable{
    private String naziv;

    // *********** Get Metode ***********
    public String dajNaziv()
    {
        return this.naziv;
    }

    // ***********Set Metode ***********
    public void postaviNaziv(String s)
    {
        this.naziv = s;
    }

    // *********** Konstruktori ***********
    public Film()
    {
        naziv = "";
    }
    public Film(String ime)
    {
        this.naziv = ime;
    }
    protected Film(Parcel parcel){naziv = parcel.readString(); }

    // *********** Metode ***********
    public String toString() { return this.dajNaziv(); }
    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>()
    {
        @Override
        public Film createFromParcel(Parcel parcel)
        {
            return new Film(parcel);
        }
        @Override
        public Film[] newArray(int size)
        {
            return new Film[size];
        }
    };
    @Override
    public int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(naziv);
    }
}
