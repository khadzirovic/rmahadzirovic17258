package com.example.kenan.filmovi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Kenan on 14.06.2017..
 */

public class GlumacDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "glumciBaza.db";
    public static final String TABLE_GLUMCI = "Glumci";
    public static final String TABLE_ZANROVI = "Zanrovi";
    public static final String TABLE_REZISERI = "Reziseri";
    public static final String TABLE_GLUMCI_REZISERI = "GlumciReziseri";
    public static final String TABLE_GLUMCI_ZANROVI = "GlumciZanrovi";
    public static final String TABLE_FILMOVI = "Filmovi";
    public static final int DATABASE_VERSION = 1;

    // ATRIBUTI
        // GLUMAC
    public static final String GLUMAC_ID = "_id";
    public static final String GLUMAC_ONLINE_ID = "online_id";
    public static final String GLUMAC_IME = "ime";
    public static final String GLUMAC_SLIKA = "slika";
    public static final String GLUMAC_SPOL = "spol";
    public static final String GLUMAC_DATUM = "datum";
    public static final String GLUMAC_MJESTO = "mjesto";
    public static final String GLUMAC_REJTING = "rejting";
    public static final String GLUMAC_URL = "url";
    public static final String GLUMAC_BIOGRAFIJA = "biografija";
        // ZANR
    public static final String ZANR_ID = "zanr_id";
    public static final String ZANR_NAZIV = "naziv_zanra";
        // REZISER
    public static final String REZISER_ID = "reziser_id";
    public static final String REZISER_IME = "reziser_ime";
        // Many-to-many
        // GLUMAC-REZISER
    public static final String GLUMAC_REZISER_GLUMAC = "glumac_id";
    public static final String GLUMAC_REZISER_REZISER = "reziser_id";
        // GLUMAC-ZANR
    public static final String GLUMAC_ZANR_GLUMAC = "glumac_id";
    public static final String GLUMAC_ZANR_ZANR = "zanr_id";

    public GlumacDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context,name,factory,version);
    }
    public GlumacDBHelper(Context context)
    {
        super(context,"glumciBaza.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + TABLE_GLUMCI +
                    "(" + GLUMAC_ID +" integer primary key autoincrement, "
                    + GLUMAC_ONLINE_ID + " integer, "
                    + GLUMAC_IME + " text not null, "
                    + GLUMAC_SLIKA +" text, "
                    + GLUMAC_SPOL +" text not null, "
                    + GLUMAC_DATUM + " text, "
                    + GLUMAC_MJESTO + " text, "
                    + GLUMAC_REJTING + " text, "
                    + GLUMAC_URL + " text, "
                    + GLUMAC_BIOGRAFIJA + " text);");
        db.execSQL("create table " + TABLE_ZANROVI + "("
                    + ZANR_ID + " integer primary key autoincrement, "
                    + ZANR_NAZIV + " text not null);");
        db.execSQL("create table " + TABLE_REZISERI +" ("
                    + REZISER_ID +" integer primary key autoincrement, "
                    + REZISER_IME +" text not null);");
        db.execSQL("create table " + TABLE_GLUMCI_ZANROVI + "( "
                    + GLUMAC_ZANR_GLUMAC + " integer not null, "
                    + GLUMAC_ZANR_ZANR + " integer not null);");
        db.execSQL("create table " + TABLE_GLUMCI_REZISERI + "("
                    + GLUMAC_REZISER_GLUMAC + " integer not null, "
                    + GLUMAC_REZISER_REZISER + " text not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZANROVI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_ZANROVI);
        onCreate(db);
    }

}
