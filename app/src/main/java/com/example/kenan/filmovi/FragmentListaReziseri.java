package com.example.kenan.filmovi;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentListaReziseri extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        return inflater.inflate(R.layout.fragment_lista_reziseri,container,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(getArguments().containsKey("Alista"))
        {
            ArrayList<Reziser> reziseri;
            reziseri = getArguments().getParcelableArrayList("Alista");
            ListView lv = (ListView)getView().findViewById(R.id.listaReziseri);
            final ArrayAdapter<Reziser> adapter;
            adapter = new ArrayAdapter<Reziser>(getActivity(),android.R.layout.simple_list_item_1, reziseri);
            lv.setAdapter(adapter);

        }
    }
}
