package com.example.kenan.filmovi;

/**
 * Created by Kenan on 15.04.2017..
 */

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentListaZanrovi extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        return inflater.inflate(R.layout.fragment_lista_zanrovi,container,false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(getArguments().containsKey("Alista"))
        {
            ArrayList<Zanr> zanrovi = new ArrayList<Zanr>();
            zanrovi = getArguments().getParcelableArrayList("Alista");
            ListView lv = (ListView)getView().findViewById(R.id.listaZanrovi);
            AdapterZanr adapter = new AdapterZanr(getActivity(),R.layout.lista_zanrova,zanrovi);
            lv.setAdapter(adapter);
        }
    }
}
