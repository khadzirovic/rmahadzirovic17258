package com.example.kenan.filmovi;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/**
 * Created by Kenan on 14.06.2017..
 */

public class AdapterFilm extends  ArrayAdapter<Film>{
    int resource;

    public AdapterFilm(Context context, int _resource, ArrayList<Film> items)
    {
        super(context, _resource, items);
        resource = _resource;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else {
            // Ukoliko je update potrebno je samo izmjeniti vrijednosti polja
            newView = (LinearLayout)convertView;
        }
        Film classInstance = getItem(position);

        //
        TextView naziv = (TextView)newView.findViewById(R.id.Ime);
        naziv.setText(classInstance.dajNaziv());
        return newView;
    }



}