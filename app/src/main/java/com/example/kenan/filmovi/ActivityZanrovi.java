package com.example.kenan.filmovi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Kenan on 31.03.2017..
 */

public class ActivityZanrovi extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zanrovi);

        ListView lista = (ListView) findViewById(R.id.listaZanrovi);

        /* HARDKODIRANI PODACI*/
        final ArrayList<Zanr> unosi = new ArrayList<Zanr>();
        unosi.add(new Zanr("Horor"));
        unosi.add(new Zanr("Komedija"));
        unosi.add(new Zanr("Triler"));
        /* KRAJ HARDKODIRANJA PODATAKA */

        final AdapterZanr adapter = new AdapterZanr(this, R.layout.lista_zanrova, unosi);
        lista.setAdapter(adapter);

        lista.setAdapter(adapter);


        Button dugmeGlumci = (Button)findViewById(R.id.buttonGlumci);
        Button dugmeReziseri = (Button)findViewById(R.id.buttonReziseri);
        Button dugmeZanrovi = (Button) findViewById(R.id.buttonZanrovi);
        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActivityZanrovi.this, MainActivity.class);
                ActivityZanrovi.this.startActivity(myIntent);
            }
        });

        dugmeReziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ActivityZanrovi.this, ActivityReziseri.class);
                ActivityZanrovi.this.startActivity(myIntent);
            }
        });
    }
}
