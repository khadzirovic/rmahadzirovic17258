package com.example.kenan.filmovi;

import android.app.Fragment;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.Locale;

/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentHomeDugmad extends Fragment {

    public interface Komunikator{
        public void onGlumciClick();
        public void onReziseriClick();
        public void onZanroviClick();
        public void onFilmoviClick();
    }

    private Komunikator kom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        return inflater.inflate(R.layout.fragment_button_home,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button dugmeGlumci = (Button)getActivity().findViewById(R.id.buttonGlumci);
        Button dugmeReziseri = (Button)getView().findViewById(R.id.buttonReziseri);
        Button dugmeZanrovi = (Button)getView().findViewById(R.id.buttonZanrovi);
        Button dugmeFilmovi = (Button)getView().findViewById(R.id.buttonFilmovi);

        ImageView ikona = (ImageView)getActivity().findViewById(R.id.imageViewJezici);

        ikona.setImageResource(getResources().getIdentifier("com.example.kenan.filmovi:drawable/"+ Locale.getDefault().getLanguage(),null,null));

        if(getResources().getIdentifier("com.example.kenan.filmovi:drawable/"+ Locale.getDefault().getLanguage(),null,null) != 0)
            Log.d("JEZIK","Pronasao");
        try {
            kom = (Komunikator) getActivity();
        }
        catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        dugmeReziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onReziseriClick();
            }
        });

        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onGlumciClick();
            }
        });

        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onZanroviClick();
            }
        });

        dugmeFilmovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onFilmoviClick();
            }
        });

    }

}
