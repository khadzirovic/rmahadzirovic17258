package com.example.kenan.filmovi;

        import android.app.Fragment;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.ListView;

/**
 * Created by Kenan on 15.04.2017..
 */

public class FragmentHomeDugmadH extends Fragment{
    public interface KomunikatorH{
        public void onGlumciClickH();
        public void onOstaloClick();
    }
    private KomunikatorH kom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        return inflater.inflate(R.layout.fragment_button_home_h,container,false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Button dugmeGlumci = (Button)getActivity().findViewById(R.id.buttonGlumci);
        Button dugmeOstalo = (Button)getView().findViewById(R.id.buttonOstalo);

        try {
            kom = (KomunikatorH) getActivity();
        }
        catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        dugmeOstalo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onOstaloClick();
            }
        });

        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kom.onGlumciClickH();
            }
        });

    }
}

