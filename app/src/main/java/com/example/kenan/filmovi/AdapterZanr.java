package com.example.kenan.filmovi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kenan on 29.03.2017..
 */

public class AdapterZanr extends ArrayAdapter<Zanr> {
    int resource;

    public AdapterZanr(Context context, int _resource, ArrayList<Zanr> items)
    {
        super(context, _resource, items);
        resource = _resource;
    }//resource je id layout-a list item-a
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Kreiranje i inflate-anje view klase
        LinearLayout newView;
        if (convertView == null) {
            // Ukoliko je ovo prvi put da se pristupa klasi convertView, odnosno nije upadate
            // Potrebno je kreirati novi objekat i inflate-at ga
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else {
            // Ukoliko je update potrebno je samo izmjeniti vrijednosti polja
            newView = (LinearLayout)convertView;
        }
        Zanr classInstance = getItem(position);

        //
        TextView ime = (TextView)newView.findViewById(R.id.nazivZanra);
        ImageView ikona = (ImageView)newView.findViewById(R.id.slikaZanra);
        ime.setText(classInstance.dajNaziv());
        //rejting.setPadding(100-(int)mjesto.getTextSize(),0,0,0);

        int resurs=getContext().getResources().getIdentifier("com.example.kenan.filmovi:drawable/"+classInstance.dajSliku(),null,null);
        if(resurs<=0) {
            resurs = R.drawable.slika;
        }
        ikona.setImageResource(resurs);
        //
        return newView;
    }

}