package com.example.kenan.filmovi;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.facebook.stetho.Stetho;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_BIOGRAFIJA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_DATUM;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_IME;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_MJESTO;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ONLINE_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REJTING;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REZISER_GLUMAC;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_REZISER_REZISER;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SLIKA;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_SPOL;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_URL;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ZANR_GLUMAC;
import static com.example.kenan.filmovi.GlumacDBHelper.GLUMAC_ZANR_ZANR;
import static com.example.kenan.filmovi.GlumacDBHelper.REZISER_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.REZISER_IME;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_GLUMCI;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_GLUMCI_REZISERI;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_GLUMCI_ZANROVI;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_REZISERI;
import static com.example.kenan.filmovi.GlumacDBHelper.TABLE_ZANROVI;
import static com.example.kenan.filmovi.GlumacDBHelper.ZANR_ID;
import static com.example.kenan.filmovi.GlumacDBHelper.ZANR_NAZIV;

public class MainActivity extends AppCompatActivity implements FragmentListaGlumci.OnItemClick, FragmentHomeDugmad.Komunikator, FragmentHomeDugmadH.KomunikatorH, SearchInfo.OnInfoSearchDone, FragmentDetaljiGlumci.Bookmark, FragmentListaFilmovi.OnItemClickFilmovi{

    final ArrayList<Glumac> glumci = new ArrayList<Glumac>();
    final ArrayList<Reziser> reziseri = new ArrayList<Reziser>();
    final ArrayList<Zanr> zanrovi = new ArrayList<Zanr>();
    final ArrayList<Film> filmovi = new ArrayList<Film>();
    FragmentManager fm;

    public void PopuniPodatke() {
        // HARDKODIRANI PODACI
        Glumac willsmith = new Glumac("2888", "Will Smith", "willsmith", "Muski", "1968", "Philadelphia", "4.5", "http://www.imdb.com/name/nm0000226/", "In west Philadelphia born and raised.");
        Glumac robertdeniro = new Glumac("380","Robert De Niro", "robertdeniro", "Muski", "1943", "New York City", "4.0", "http://www.imdb.com/name/nm0000134/", "Robert De Niro, thought of as one of the greatest actors of all time, was born in Greenwich Village, Manhattan, New York City, to artists Virginia and Robert De Niro Sr.");
        Glumac merylstreep = new Glumac("5064", "Meryl Streep", "merylstreep", "Zenski", "1949", "Summit", "4.5", "http://www.imdb.com/name/nm0000658/", "Considered by many critics to be the greatest living actress, Meryl Streep has been nominated for the Academy Award an astonishing 20 times, and has won it three times. ");
        Glumac bradpitt = new Glumac("287", "Brad Pitt", "bradpitt", "Muski", "1963", "Shawnee", "4.0", "http://www.imdb.com/name/nm0000093", "An actor and producer known as much for his versatility as he is for his handsome face, Golden Globe-winner.");
        Glumac angelinajolie = new Glumac("11701", "Angelina Jolie", "angelinajolie", "Zenski", "1975", "Los Angeles", "3.5", "http://www.imdb.com/name/nm0001401/", "Angelina Jolie is an Oscar-winning actress who became popular after playing the title role in the \"Lara Croft\" blockbuster movies, as well as Mr. & Mrs. Smith (2005), Wanted (2008), Salt (2010) and Maleficent (2014).");
        Glumac nicolekidman = new Glumac("2227","Nicole Kidman", "test", "Zenski", "1967", "Honolulu", "3.5", "http://www.imdb.com/name/nm0000173/", "Elegant blonde Nicole Kidman, known as one of Hollywood's top Australian imports, was actually born in Honolulu, Hawaii, while her Australian parents were there on educational visas.");
        Glumac jamesavery = new Glumac("51547","James Avery", "jamesavery", "Muski", "1945 - 2013", "Pughsville", "4.0", "http://www.imdb.com/name/nm0043041/", "Although best known as the uncle/patriarch and judge \"Philip Banks\" on The Fresh Prince of Bel-Air (1990), James Avery is a classically trained actor and scholar.");

        glumci.add(willsmith);
        glumci.add(robertdeniro);
        glumci.add(merylstreep);
        glumci.add(bradpitt);
        glumci.add(angelinajolie);
        glumci.add(nicolekidman);
        glumci.add(jamesavery);
        // KRAJ HARDKODIRANJA PODATAKA

        /* HARDKODIRANI PODACI*/
        Reziser r1 = new Reziser("Quentin Tarantino");
        Reziser r2 = new Reziser("Brian De Palma");
        Reziser r3 = new Reziser("Stephen Spielberg");
        reziseri.add(r1);
        reziseri.add(r2);
        reziseri.add(r3);
        /* KRAJ HARDKODIRANJA PODATAKA */

        /* HARDKODIRANI PODACI*/
        zanrovi.add(new Zanr("Horor"));
        zanrovi.add(new Zanr("Komedija"));
        zanrovi.add(new Zanr("Triler"));
        /* KRAJ HARDKODIRANJA PODATAKA */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Stetho.initializeWithDefaults(this);
        PopuniPodatke();
        //ocistiBazu();
        //korigujBazu();
        fm = getFragmentManager();

        FrameLayout w = (FrameLayout)findViewById(R.id.mjestoL1);

        if(w==null) {
            prikaziDugmad();
            prikaziGlumce();
        }
        else
        {
            prikaziVodoravno();
        }
    }

    private void prikaziVodoravno()
    {
        prikaziHorizontalnuDugmad();
        prikaziListeHorizontalno();
    }

    private void prikaziHorizontalnuDugmad()
    {
        FragmentHomeDugmadH fd = new FragmentHomeDugmadH();
        fm.beginTransaction().replace(R.id.mjestoDugmad, fd).commit();
    }

    private void prikaziListeHorizontalno()
    {
        fm = getFragmentManager();
        FragmentListaGlumci fl = (FragmentListaGlumci) fm.findFragmentById(R.id.mjestoL1);
        if (fl == null) {
            fl = new FragmentListaGlumci();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", glumci);
            fl.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoL1, fl).commit();
        }
        else {
            fm.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        onItemClicked(0);
    }

    private void prikaziDugmad()
    {
        FragmentHomeDugmad fd = new FragmentHomeDugmad();
        fm.beginTransaction().replace(R.id.mjestoDugmad, fd).commit();
    }

    private void prikaziGlumce()
    {
        fm = getFragmentManager();
        FragmentListaGlumci fl = (FragmentListaGlumci) fm.findFragmentById(R.id.mjestoLista);
        if (fl == null) {
            fl = new FragmentListaGlumci();
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("Alista", glumci);
            fl.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.mjestoLista, fl).commit();
        }
        else {
            fm.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
    @Override
    public void onItemClicked(int pos) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("glumac", glumci.get(pos));
        FragmentDetaljiGlumci fd = new FragmentDetaljiGlumci();
        fd.setArguments(arguments);
        if(findViewById(R.id.mjestoLista)!=null) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoLista, fd).addToBackStack(null).commit();
        }
        else {
            getFragmentManager().beginTransaction().replace(R.id.mjestoL2, fd).addToBackStack(null).commit();

            FrameLayout layout = (FrameLayout)findViewById(R.id.mjestoL2);
            layout.setBackgroundColor(glumci.get(pos).dajBoju());
        }

        // pretraga po ID:u
        new SearchInfo((SearchInfo.OnInfoSearchDone)MainActivity.this).execute(glumci.get(pos).dajId());

    }

    @Override
    public void OnItemFilmoviClicked(int pos) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("film", filmovi.get(pos));
        FragmentDetaljiFilmovi fd = new FragmentDetaljiFilmovi();
        fd.setArguments(arguments);
        getFragmentManager().beginTransaction().replace(R.id.mjestoLista, fd).addToBackStack(null).commit();
    }


    @Override
    public void onReziseriClick()
    {
        fm = getFragmentManager();
        FragmentListaReziseri fl = new FragmentListaReziseri();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", reziseri);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoLista, fl).addToBackStack(null).commit();
    }

    @Override
    public void onGlumciClick() {
        fm = getFragmentManager();
        FragmentListaGlumci fl = new FragmentListaGlumci();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", glumci);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoLista, fl).addToBackStack(null).commit();
    }
    @Override
    public void onZanroviClick()
    {
        fm = getFragmentManager();
        FragmentListaZanrovi fl = new FragmentListaZanrovi();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", zanrovi);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoLista, fl).addToBackStack(null).commit();
    }
    @Override
    public void onFilmoviClick()
    {
        fm = getFragmentManager();
        FragmentListaFilmovi fl = new FragmentListaFilmovi();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", filmovi);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoLista, fl).addToBackStack(null).commit();
    }
    @Override
    public void onOstaloClick()
    {
        fm = getFragmentManager();
        FragmentListaZanrovi fl = new FragmentListaZanrovi();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", zanrovi);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoL1, fl).addToBackStack(null).commit();

        fm = getFragmentManager();
        FragmentListaReziseri fr = new FragmentListaReziseri();
        arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", reziseri);
        fr.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoL2, fr).addToBackStack(null).commit();

        FrameLayout layout = (FrameLayout)findViewById(R.id.mjestoL2);
        FrameLayout layout1 = (FrameLayout)findViewById(R.id.mjestoL1);
        layout.setBackgroundColor(layout1.getSolidColor());
    }


    @Override
    public void onGlumciClickH()
    {
        fm = getFragmentManager();
        FragmentListaGlumci fl = new FragmentListaGlumci();
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("Alista", glumci);
        fl.setArguments(arguments);
        fm.beginTransaction().replace(R.id.mjestoL1, fl).addToBackStack(null).commit();

        arguments = new Bundle();
        arguments.putParcelable("glumac", glumci.get(0));
        FragmentDetaljiGlumci fd = new FragmentDetaljiGlumci();
        fd.setArguments(arguments);
        getFragmentManager().beginTransaction().replace(R.id.mjestoL2,fd).addToBackStack(null).commit();
    }

    @Override
    public void onDone(ArrayList<Reziser> r, ArrayList<Zanr> z)
    {
        zanrovi.clear();
        reziseri.clear();
        zanrovi.addAll(z);
        reziseri.addAll(r);
    }

    @Override
    public void dodajBookmarkano(Glumac g)
    {
        SQLiteDatabase db = new GlumacDBHelper(this).getWritableDatabase();
        for(int i = 0; i < zanrovi.size();i++) {
            if(slobodanZanr(zanrovi.get(i).dajNaziv()))
            {
                ContentValues noveVrijednosti = new ContentValues();
                noveVrijednosti.put(ZANR_NAZIV, zanrovi.get(i).dajNaziv());
                db.insert(GlumacDBHelper.TABLE_ZANROVI, null, noveVrijednosti);
            }
            ContentValues pomocneVrijednosti = new ContentValues();
            pomocneVrijednosti.put(GLUMAC_ZANR_GLUMAC, g.dajId());
            pomocneVrijednosti.put(GLUMAC_ZANR_ZANR,zanrovi.get(i).dajNaziv());
            db.insert(GlumacDBHelper.TABLE_GLUMCI_ZANROVI, null,pomocneVrijednosti);
        }
        for(int i = 0; i < reziseri.size();i++) {
            if(slobodanReziser(reziseri.get(i).dajIme()))
            {
                ContentValues noveVrijednosti = new ContentValues();
                noveVrijednosti.put(REZISER_IME, reziseri.get(i).dajIme());
                db.insert(GlumacDBHelper.TABLE_REZISERI, null, noveVrijednosti);
            }
            ContentValues pomocneVrijednosti = new ContentValues();
            pomocneVrijednosti.put(GLUMAC_REZISER_GLUMAC, g.dajId());
            pomocneVrijednosti.put(GLUMAC_REZISER_REZISER,reziseri.get(i).dajIme());
            db.insert(GlumacDBHelper.TABLE_GLUMCI_REZISERI,null,pomocneVrijednosti);

        }
    }

    public Boolean slobodanZanr(String zanr)
    {
        String[] koloneRezulat = new String[]{ ZANR_ID, ZANR_NAZIV};
// Specificiramo WHERE dio upita
        String where = ZANR_NAZIV + "='"+zanr+"'";
// Definišemo argumente u where upitu, group by, having i order po potrebi
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;
// Dohvatimo referencu na bazu (poslije ćemo opisati kako se implementira helper)
        SQLiteDatabase db = new GlumacDBHelper(this).getWritableDatabase();
// Izvršimo upit
        Cursor cursor = db.query(GlumacDBHelper.TABLE_ZANROVI,koloneRezulat, where, whereArgs, groupBy, having, order);
        //Log.d("LOG: "," Pronadjeno :" + cursor.getCount() + " za zanr " + zanr);
        if(cursor.getCount() != 0)
            return false;
        else return true;

    }
    public Boolean slobodanReziser(String reziser)
    {
        String[] koloneRezulat = new String[]{ REZISER_ID, REZISER_IME};
// Specificiramo WHERE dio upita
        String where = REZISER_IME + "='"+reziser+"'";
// Definišemo argumente u where upitu, group by, having i order po potrebi
        String whereArgs[] = null;
        String groupBy = null;
        String having = null;
        String order = null;
// Dohvatimo referencu na bazu (poslije ćemo opisati kako se implementira helper)
        SQLiteDatabase db = new GlumacDBHelper(this).getWritableDatabase();
// Izvršimo upit
        Cursor cursor = db.query(GlumacDBHelper.TABLE_REZISERI,koloneRezulat, where, whereArgs, groupBy, having, order);
        //Log.d("LOG: "," Pronadjeno :" + cursor.getCount() + " za zanr " + zanr);
        if(cursor.getCount() != 0)
            return false;
        else return true;
    }
    public void ocistiBazu()
    {
        SQLiteDatabase db = new GlumacDBHelper(this).getWritableDatabase();
        db.delete(TABLE_REZISERI,"",null);
        db.delete(TABLE_GLUMCI,null,null);
        db.delete(TABLE_ZANROVI,null,null);
        //db.delete(TABLE_GLUMCI_REZISERI,null,null);
        //db.delete(TABLE_GLUMCI_ZANROVI,null,null);
        //db.delete(TABLE_FILMOVI,null,null);
    }
    public void korigujBazu()
    {
        SQLiteDatabase db = new GlumacDBHelper(this).getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_ZANROVI);
        db.execSQL("create table " + TABLE_GLUMCI_ZANROVI + "( "
                + GLUMAC_ZANR_GLUMAC + " integer not null, "
                + GLUMAC_ZANR_ZANR + " text not null);");
        db.execSQL("create table " + TABLE_GLUMCI_REZISERI + "("
                + GLUMAC_REZISER_GLUMAC + " integer not null, "
                + GLUMAC_REZISER_REZISER + " text not null);");
    }
}
