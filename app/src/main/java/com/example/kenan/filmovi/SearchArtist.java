package com.example.kenan.filmovi;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Kenan on 17.05.2017..
 */

public class SearchArtist extends AsyncTask<String, Integer, Void> {
    public interface OnGlumacSearchDone
    {
        public void onDone(ArrayList<Glumac> rez);
    }
    ArrayList<Glumac> rez;
    private OnGlumacSearchDone pozivatelj;
    public SearchArtist(OnGlumacSearchDone p){pozivatelj = p;}
    @Override
    protected Void doInBackground(String... params)
    {
        String query = null;
        try
        {
            query = URLEncoder.encode(params[0],"utf-8");
        }
        /*catch(MalformedURLException e)
        {
            e.printStackTrace();
        }*/
        catch(UnsupportedEncodingException e)
        {
            // :(
        }

        String url1 = "https://api.themoviedb.org/3/search/person?api_key=ccf4aada7fe42c0ab1d8f42356aad70f&query=" + query;
        try
        {
            rez = new ArrayList<Glumac>();

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("results");

            for(int i = 0; i<items.length(); i++)
            {
                //
                JSONObject artist = items.getJSONObject(i);
                String id = artist.getString("id");
                String personURL = "https://api.themoviedb.org/3/person/" + id + "?api_key=ccf4aada7fe42c0ab1d8f42356aad70f";

                URL glumciURL = new URL(personURL);
                HttpURLConnection urlConnection2 = (HttpURLConnection)glumciURL.openConnection();
                InputStream stream = new BufferedInputStream(urlConnection2.getInputStream());
                String rezultatOsoba = convertStreamToString(stream);

                JSONObject glumac = new JSONObject(rezultatOsoba);
                //String id = glumac.getString("id");
                String name = glumac.getString("name");
                String[] polovi = {"", "Zenski", "Muski"};
                String pol = polovi[glumac.getInt("gender")];
                String mjesto = glumac.getString("place_of_birth");
                String homepage = glumac.getString("homepage");
                String biografija = glumac.getString("biography");
                String godiste = glumac.getString("birthday");
                String rejting = glumac.getString("popularity");
                String slika = "https://image.tmdb.org/t/p/w640" + glumac.getString("profile_path");

                // name, spol, godiste, mjesto, rejting, url, homepage
                rez.add(new Glumac(id, name, slika, pol, godiste, mjesto, rejting, homepage, biografija));

                //

            }
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(rez);
    }

    public String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        }
        catch(IOException e)
        {

        }
        finally
        {
            try
            {
                is.close();
            }
            catch(IOException e)
            {

            }
        }
        return sb.toString();
    }
}
