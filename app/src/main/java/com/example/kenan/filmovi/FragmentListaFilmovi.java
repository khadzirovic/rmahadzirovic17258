package com.example.kenan.filmovi;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kenan on 14.06.2017..
 */

public class FragmentListaFilmovi extends Fragment implements SearchMovie.OnFilmSearchDone{

    public interface OnItemClickFilmovi{
        public void OnItemFilmoviClicked(int pos);
    }
    ArrayList<Film> filmovi;
    ArrayAdapter<Film> adapter;
    private OnItemClickFilmovi oic;

    @Override
    public void onDone(ArrayList<Film> f)
    {
        filmovi.clear();
        filmovi.addAll(f);
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstancestate){
        View iv = inflater.inflate(R.layout.fragment_lista_filmovi,container,false);
        final Button dugme = (Button)iv.findViewById(R.id.buttonTrazi);
        final EditText text = (EditText)iv.findViewById(R.id.editText);

        dugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SearchMovie((SearchMovie.OnFilmSearchDone) FragmentListaFilmovi.this).execute(text.getText().toString());
                text.setText("");
            }
        });
        return iv;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(getArguments().containsKey("Alista"))
        {
            filmovi = new ArrayList<Film>();
            filmovi = getArguments().getParcelableArrayList("Alista");
            ListView lv = (ListView)getView().findViewById(R.id.listaFilmovi);
            adapter = new ArrayAdapter<Film>(getActivity(),android.R.layout.simple_list_item_1,filmovi);
            lv.setAdapter(adapter);


            try{
                oic = (FragmentListaFilmovi.OnItemClickFilmovi)getActivity();
            }
            catch(ClassCastException e){
                throw new ClassCastException(getActivity().toString()+"Treba implementirati OnItemFilmoviClicked");
            }

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                    oic.OnItemFilmoviClicked(position);
                }
            });
        }
    }
}
