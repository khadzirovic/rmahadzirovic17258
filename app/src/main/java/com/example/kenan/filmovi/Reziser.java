package com.example.kenan.filmovi;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kenan on 31.03.2017..
 */

public class Reziser implements Parcelable {
    // *********** Atributi ***********
    private String imePrezime;

    // *********** Get Metode ***********
    public String dajIme()
    {
        return this.imePrezime;
    }

    // ***********Set Metode ***********
    public void postaviIme(String s)
    {
        this.imePrezime = s;
    }

    // *********** Konstruktori ***********
    public Reziser()
    {
        imePrezime = "";
    }
    public Reziser(String ime)
    {
        this.imePrezime = ime;
    }
    protected Reziser(Parcel parcel){imePrezime = parcel.readString(); }

    // *********** Metode ***********
    public String toString() { return this.dajIme(); }
    public static final Parcelable.Creator<Reziser> CREATOR = new Parcelable.Creator<Reziser>()
    {
        @Override
        public Reziser createFromParcel(Parcel parcel)
        {
            return new Reziser(parcel);
        }
        @Override
        public Reziser[] newArray(int size)
        {
            return new Reziser[size];
        }
    };
    @Override
    public int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(imePrezime);
    }
}